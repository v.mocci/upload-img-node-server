## About

This project was made to test an RESTFUL image upload system with Node.js, not for money purposes.

# Technologies

- Node.js/JavaScript;
- MongoDB (for offline uploads);
- AWS S3 (for online uploads);
- Insonmia/Postman (for request tests);

# Usage

Run the script "yarn dev", for yarn users, to make the project online on port 3000. You can check and test these endpoints:

- List all entries: (GET requisition) http://localhost:3000/posts
- Send an entry: (POST requisition) http://localhost:3000/posts
- Send a delete request: (DELETE requisition) http://localhost:3000/posts/5ec0860157b6e242d0c1d5e6 (the last one is the identifier)

# Aditional Notes

The "node_modules" folder should be deleted from the repository in future commits, it was just a test.